# UXF Content

## Install
```
$ composer req uxf/content
```

## ContentSearchHelper

```php
function search(string $term) {
    $qb = $this->entityManager->createQueryBuilder()
        ->select('article')
        ->from(Article::class, 'article');

    $qb = ContentSearchHelper::apply($qb, 'article.content.textSearch', $text, ContentSearchHelper::EQUALS);

    return $qb->getQuery()->getResult();
}
```

### Doctrine migration

```php
$this->addSql("CREATE TEXT SEARCH DICTIONARY unaccented_czech (template = ispell, dictfile = 'unaccented_czech', afffile = 'unaccented_czech', stopwords = 'unaccented_czech')");
$this->addSql("CREATE TEXT SEARCH CONFIGURATION unaccented_cs (copy = simple)");
$this->addSql("ALTER TEXT SEARCH CONFIGURATION unaccented_cs ALTER MAPPING FOR asciiword, asciihword, hword_asciipart, word, hword, hword_part WITH unaccented_czech");
```

### GIN index

```sql
-- original
CREATE INDEX IDX_42803DB7DCC6AB0B ON app_blog.test_article (content_search);

-- modified
CREATE INDEX IDX_42803DB7DCC6AB0B ON app_blog.test_article USING GIN(content_search);
```
