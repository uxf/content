<?php

declare(strict_types=1);

namespace UXF\Content\Controller\Cms;

use UXF\Content\Entity\Content;
use UXF\Content\Http\Cms\ContentCmsRequestBody;
use UXF\Content\Http\Cms\ContentCmsResponse;
use UXF\Content\Service\ContentService;
use UXF\Core\Attribute\FromBody;

final readonly class UpdateContentController
{
    public function __construct(
        private ContentService $contentService,
    ) {
    }

    public function __invoke(Content $content, #[FromBody] ContentCmsRequestBody $body): ContentCmsResponse
    {
        return ContentCmsResponse::create($this->contentService->update($content, $body));
    }
}
