<?php

declare(strict_types=1);

namespace UXF\Content\Controller\Cms;

use UXF\Content\Entity\Content;
use UXF\Content\Http\Cms\ContentCmsResponse;

final readonly class GetContentController
{
    public function __invoke(Content $content): ContentCmsResponse
    {
        return ContentCmsResponse::create($content);
    }
}
