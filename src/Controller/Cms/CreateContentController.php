<?php

declare(strict_types=1);

namespace UXF\Content\Controller\Cms;

use UXF\Content\Http\Cms\ContentCmsRequestBody;
use UXF\Content\Http\Cms\ContentCmsResponse;
use UXF\Content\Service\ContentService;
use UXF\Core\Attribute\FromBody;

final readonly class CreateContentController
{
    public function __construct(
        private ContentService $contentService,
    ) {
    }

    public function __invoke(#[FromBody] ContentCmsRequestBody $body): ContentCmsResponse
    {
        return ContentCmsResponse::create($this->contentService->create($body));
    }
}
