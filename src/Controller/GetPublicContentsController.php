<?php

declare(strict_types=1);

namespace UXF\Content\Controller;

use UXF\Content\Http\ContentRequestQuery;
use UXF\Content\Http\ContentResponse;
use UXF\Content\Service\ContentResponseProvider;
use UXF\Core\Attribute\FromQuery;

final readonly class GetPublicContentsController
{
    public function __construct(private ContentResponseProvider $contentResponseProvider)
    {
    }

    /**
     * @return ContentResponse[]
     */
    public function __invoke(#[FromQuery] ContentRequestQuery $query): array
    {
        return $this->contentResponseProvider->provide($query);
    }
}
