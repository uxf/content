<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ReadableCollection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\DateTime;
use UXF\Storage\Entity\Image;

#[ORM\Entity]
#[ORM\Table(schema: 'uxf_content')]
#[ORM\UniqueConstraint(columns: ['type', 'seo_name'])]
class Content
{
    #[ORM\Column, ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private int $id;

    #[ORM\Column]
    private string $type;

    #[ORM\Column]
    private string $name;

    #[ORM\Column(type: 'tsvector')]
    private string $nameSearch;

    #[ORM\Column(type: 'text')]
    private string $perex;

    // seo*** -> pripraveno kdyby byla podpora ManyToOne v Embedded
    #[ORM\Column(nullable: true)]
    private ?string $seoName;

    #[ORM\Column]
    private string $seoTitle;

    #[ORM\Column]
    private string $seoDescription;

    #[ORM\Column]
    private string $seoOgTitle;

    #[ORM\Column]
    private string $seoOgDescription;

    #[ORM\ManyToOne]
    private ?Image $seoOgImage;

    #[ORM\Column]
    private VisibilityLevel $visibilityLevel;

    #[ORM\Column]
    private bool $hidden;

    #[ORM\ManyToOne]
    private ?Image $image;

    #[ORM\Column(type: DateTime::class, nullable: true)]
    private ?DateTime $publishedAt;

    #[ORM\ManyToOne]
    private ?Author $author;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $createdAt;

    #[ORM\Column(type: DateTime::class)]
    private DateTime $updatedAt;

    #[ORM\Embedded]
    private ContentLite $content;

    /**
     * @var Collection<int, Tag>
     */
    #[ORM\ManyToMany(targetEntity: Tag::class)]
    #[ORM\JoinTable(name: 'content_x_tag', schema: 'uxf_content')]
    private Collection $tags;

    #[ORM\ManyToOne]
    private ?Content $parent;

    #[ORM\ManyToOne]
    private ?Category $category;

    /**
     * @param Tag[] $tags
     */
    public function __construct(
        string $type,
        string $name,
        string $perex,
        Seo $seo,
        VisibilityLevel $visibilityLevel,
        bool $hidden,
        ?Image $image,
        ?DateTime $publishedAt,
        ?Author $author,
        ContentLite $content,
        array $tags,
        ?self $parent,
        ?Category $category,
    ) {
        $this->id = 0;
        $this->type = $type;
        $this->name = $name;
        $this->nameSearch = Strings::lower(Strings::toAscii($name));
        $this->perex = $perex;
        $this->setSeo($seo);
        $this->image = $image;
        $this->publishedAt = $publishedAt;
        $this->visibilityLevel = $visibilityLevel;
        $this->hidden = $hidden;
        $this->author = $author;
        $this->content = $content;
        $this->tags = new ArrayCollection($tags);
        $this->createdAt = Clock::now();
        $this->updatedAt = Clock::now();
        $this->parent = $parent;
        $this->category = $category;
    }

    /**
     * @param Tag[] $tags
     */
    public function update(
        string $name,
        string $perex,
        Seo $seo,
        VisibilityLevel $visibilityLevel,
        bool $hidden,
        ?Image $image,
        ?DateTime $publishedAt,
        ?Author $author,
        ContentLite $content,
        array $tags,
        ?self $parent,
        ?Category $category,
    ): void {
        $this->name = $name;
        $this->nameSearch = Strings::lower(Strings::toAscii($name));
        $this->perex = $perex;
        $this->setSeo($seo);
        $this->visibilityLevel = $visibilityLevel;
        $this->hidden = $hidden;
        $this->image = $image;
        $this->publishedAt = $publishedAt;
        $this->author = $author;
        $this->content = $content;
        $this->tags = new ArrayCollection($tags);
        $this->updatedAt = Clock::now();
        $this->parent = $parent;
        $this->category = $category;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNameSearch(): string
    {
        return $this->nameSearch;
    }

    public function getPerex(): string
    {
        return $this->perex;
    }

    public function getSeo(): Seo
    {
        return new Seo(
            name: $this->seoName,
            title: $this->seoTitle,
            description: $this->seoDescription,
            ogTitle: $this->seoOgTitle,
            ogDescription: $this->seoOgDescription,
            ogImage: $this->seoOgImage,
        );
    }

    public function getVisibilityLevel(): VisibilityLevel
    {
        return $this->visibilityLevel;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function getPublishedAt(): ?DateTime
    {
        return $this->publishedAt;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return array<mixed>
     */
    public function getContent(): array
    {
        return $this->content->data;
    }

    /**
     * @return ReadableCollection<int, Tag>
     */
    public function getTags(): ReadableCollection
    {
        return $this->tags;
    }

    /**
     * @param array<Tag>|Collection<int, Tag> $tags
     */
    public function setTags(Collection|array $tags): void
    {
        $this->tags->clear();
        array_map($this->tags->add(...), $tags instanceof Collection ? $tags->getValues() : $tags);
    }

    private function setSeo(Seo $seo): void
    {
        $this->seoName = trim($seo->name ?? '') === '' ? null : $seo->name;
        $this->seoTitle = $seo->title;
        $this->seoDescription = $seo->description;
        $this->seoOgTitle = $seo->ogTitle;
        $this->seoOgDescription = $seo->description;
        $this->seoOgImage = $seo->ogImage;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * @deprecated
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }
}
