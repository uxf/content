<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

#[ORM\Embeddable]
readonly class ContentLite
{
    /**
     * @var array<mixed>
     */
    #[ORM\Column(type: 'uxf_json', options: [
        'jsonb' => true,
    ])]
    public array $data;

    #[ORM\Column(type: 'tsvector')]
    private string $search; // @phpstan-ignore-line

    #[ORM\Column(type: 'uxf_search')]
    private string $textSearch; // @phpstan-ignore-line

    /**
     * @param array<mixed> $data
     */
    public function __construct(array $data, string $search)
    {
        $this->data = $data;
        $this->search = Strings::lower(Strings::toAscii($search));

        $search = Strings::replace($search, '/[^\w0-9\-\.@+\/]+/i', ' ', utf8: true);
        $search = Strings::replace($search, '/\s+/', ' ');
        $search = trim($search);
        $this->textSearch = " {$search} ";
    }

    /**
     * @return array<array{type: string, content: mixed}>
     */
    public function getData(): array
    {
        return $this->data;
    }
}
