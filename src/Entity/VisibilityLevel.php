<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

enum VisibilityLevel: string
{
    case PUBLIC = 'PUBLIC';
    case PUBLIC_WITHOUT_SITEMAP = 'PUBLIC_WITHOUT_SITEMAP';
    case PRIVATE = 'PRIVATE';
}
