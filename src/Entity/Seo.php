<?php

declare(strict_types=1);

namespace UXF\Content\Entity;

use UXF\Storage\Entity\Image;

final readonly class Seo
{
    public function __construct(
        public ?string $name,
        public string $title,
        public string $description,
        public string $ogTitle,
        public string $ogDescription,
        public ?Image $ogImage,
    ) {
    }
}
