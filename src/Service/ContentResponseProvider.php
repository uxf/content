<?php

declare(strict_types=1);

namespace UXF\Content\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr;
use Nette\Utils\Strings;
use Symfony\Bundle\SecurityBundle\Security;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Content\Http\ContentRequestQuery;
use UXF\Content\Http\ContentResponse;

final readonly class ContentResponseProvider
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security,
    ) {
    }

    /**
     * @return ContentResponse[]
     */
    public function provide(ContentRequestQuery $query): array
    {
        $qb = $this->entityManager->createQueryBuilder()
            ->select('content')
            ->from(Content::class, 'content')
            ->andWhere('content.hidden = FALSE')
            ->setMaxResults($query->limit)
            ->setFirstResult($query->offset);

        if ($this->security->getUser() === null) {
            $qb->andWhere('content.visibilityLevel != :visibilityLevel')
                ->setParameter('visibilityLevel', VisibilityLevel::PRIVATE->value);
        }

        if ($query->types !== []) {
            $qb->andWhere('content.type IN (:types)')
                ->setParameter('types', $query->types);
        }

        if ($query->tags !== []) {
            // fix pagination (limit + offset issue)
            $tagDql = $this->entityManager->createQueryBuilder()
                ->select('contentX.id')
                ->from(Content::class, 'contentX')
                ->join('contentX.tags', 'tag')
                ->where('tag.id IN (:tags)')
                ->getQuery()
                ->getDQL();

            $qb->andWhere((new Expr())->in('content.id', $tagDql))
                ->setParameter('tags', $query->tags);
        }

        if ($query->authors !== []) {
            $qb->andWhere('content.author IN (:authors)')
                ->setParameter('authors', $query->authors);
        }

        if ($query->parent !== null) {
            $qb->andWhere('content.parent = :parent')
                ->setParameter('parent', $query->parent);
        }

        $term = Strings::lower(Strings::toAscii($query->term));
        if ($term !== '') {
            $qb
                ->andWhere('
                    TS_QUERY(content.nameSearch, :term) = TRUE OR
                    TS_QUERY(content.content.search, :term) = TRUE OR
                    UNACCENT(content.name) LIKE :likeTerm
                ')
                ->orderBy('TS_RANK(content.nameSearch, :term) * 2 + TS_RANK(content.content.search, :term)', 'DESC')
                ->setParameter('term', $term)
                ->setParameter('likeTerm', "%$term%");
        } else {
            $qb->orderBy('content.' . $query->sort->value, $query->dir->value);
        }

        return array_map(static fn (Content $content) => ContentResponse::create($content), $qb->getQuery()->getResult());
    }
}
