<?php

declare(strict_types=1);

namespace UXF\Content\Service;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Content\Entity\Content;
use UXF\Content\Http\Cms\ContentCmsRequestBody;
use UXF\Core\Exception\ValidationException;

final readonly class ContentService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function create(ContentCmsRequestBody $body): Content
    {
        $this->checkUniqueSeoName(null, $body);

        $content = new Content(
            type: $body->type,
            name: $body->name,
            perex: $body->perex,
            seo: $body->seo,
            visibilityLevel: $body->visibilityLevel,
            hidden: $body->hidden,
            image: $body->image,
            publishedAt: $body->publishedAt,
            author: $body->author,
            content: $body->content,
            tags: $body->tags,
            parent: $body->parent,
            category: $body->category,
        );
        $this->entityManager->persist($content);
        $this->entityManager->flush();
        return $content;
    }

    public function update(Content $content, ContentCmsRequestBody $body): Content
    {
        $this->checkUniqueSeoName($content, $body);

        $content->update(
            name: $body->name,
            perex: $body->perex,
            seo: $body->seo,
            visibilityLevel: $body->visibilityLevel,
            hidden: $body->hidden,
            image: $body->image,
            publishedAt: $body->publishedAt,
            author: $body->author,
            content: $body->content,
            tags: $body->tags,
            parent: $body->parent,
            category: $body->category,
        );
        $this->entityManager->flush();
        return $content;
    }

    public function checkUniqueSeoName(?Content $content, ContentCmsRequestBody $body): void
    {
        $seoName = $body->seo->name;
        if (trim($seoName ?? '') === '') {
            return;
        }

        $qb = $this->entityManager->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from(Content::class, 'c')
            ->where('c.seoName = :newSeoName')
            ->andWhere('c.type = :type')
            ->setParameter('newSeoName', $seoName);

        if ($content !== null) {
            $qb->andWhere('c.id != :id')
                ->setParameter('id', $content->getId())
                ->setParameter('type', $content->getType());
        } else {
            $qb->setParameter('type', $body->type);
        }

        if ($qb->getQuery()->getSingleScalarResult() > 0) {
            throw new ValidationException([
                [
                    'field' => 'seo.name',
                    'message' => 'Zadané SEO name už bylo použito',
                ],
            ]);
        }
    }

    public function deleteContent(Content $content): void
    {
        $this->entityManager->remove($content);
        $this->entityManager->flush();
    }
}
