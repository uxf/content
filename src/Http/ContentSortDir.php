<?php

declare(strict_types=1);

namespace UXF\Content\Http;

enum ContentSortDir: string
{
    case ASC = 'asc';
    case DESC = 'desc';
}
