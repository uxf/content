<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Seo;
use UXF\Storage\Entity\Image;
use UXF\Storage\Http\Response\ImageResponse;

final readonly class SeoResponse
{
    public function __construct(
        public ?string $name,
        public string $title,
        public string $description,
        public string $ogTitle,
        public string $ogDescription,
        public ?ImageResponse $ogImage,
    ) {
    }

    public static function create(Seo $seo, ?Image $fallbackImage): self
    {
        return new self(
            name: $seo->name,
            title: $seo->title,
            description: $seo->description,
            ogTitle: $seo->ogTitle,
            ogDescription: $seo->ogDescription,
            ogImage: ImageResponse::createNullable($seo->ogImage ?? $fallbackImage),
        );
    }
}
