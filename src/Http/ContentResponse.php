<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Content;
use UXF\Content\Entity\Tag;
use UXF\Core\Type\DateTime;
use UXF\Storage\Http\Response\ImageResponse;

// TODO Bee -> add category or categories
final readonly class ContentResponse
{
    /**
     * @param array<mixed> $content
     * @param TagResponse[] $tags
     */
    public function __construct(
        public int $id,
        public string $type,
        public string $name,
        public string $perex,
        public SeoResponse $seo,
        public ?ImageResponse $image,
        public ?DateTime $publishedAt,
        public ?AuthorResponse $author,
        public array $content,
        public array $tags,
    ) {
    }

    public static function create(Content $content): self
    {
        return new self(
            id: $content->getId(),
            type: $content->getType(),
            name: $content->getName(),
            perex: $content->getPerex(),
            seo: SeoResponse::create($content->getSeo(), $content->getImage()),
            image: ImageResponse::createNullable($content->getImage()),
            publishedAt: $content->getPublishedAt(),
            author: AuthorResponse::createNullable($content->getAuthor()),
            content: $content->getContent(),
            tags: $content->getTags()->map(static fn (Tag $tag) => TagResponse::create($tag))->getValues(),
        );
    }
}
