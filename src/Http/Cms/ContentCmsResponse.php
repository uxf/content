<?php

declare(strict_types=1);

namespace UXF\Content\Http\Cms;

use UXF\Content\Entity\Content;
use UXF\Content\Entity\Tag;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Content\Http\AuthorResponse;
use UXF\Content\Http\CategoryResponse;
use UXF\Content\Http\SeoResponse;
use UXF\Content\Http\TagResponse;
use UXF\Core\Type\DateTime;
use UXF\Storage\Http\Response\ImageResponse;

final readonly class ContentCmsResponse
{
    /**
     * @param array<mixed> $content
     * @param TagResponse[] $tags
     */
    public function __construct(
        public int $id,
        public string $type,
        public string $name,
        public string $perex,
        public SeoResponse $seo,
        public VisibilityLevel $visibilityLevel,
        public bool $hidden,
        public ?ImageResponse $image,
        public ?DateTime $publishedAt,
        public ?AuthorResponse $author,
        public ?ContentCmsPreviewResponse $parent,
        public ?CategoryResponse $category,
        public array $content,
        public array $tags,
        public DateTime $createdAt,
        public DateTime $updatedAt,
    ) {
    }

    public static function create(Content $content): self
    {
        return new self(
            id: $content->getId(),
            type: $content->getType(),
            name: $content->getName(),
            perex: $content->getPerex(),
            seo: SeoResponse::create($content->getSeo(), null),
            visibilityLevel: $content->getVisibilityLevel(),
            hidden: $content->isHidden(),
            image: ImageResponse::createNullable($content->getImage()),
            publishedAt: $content->getPublishedAt(),
            author: AuthorResponse::createNullable($content->getAuthor()),
            parent: ContentCmsPreviewResponse::createNullable($content->getParent()),
            category: CategoryResponse::createNullable($content->getCategory()),
            content: $content->getContent(),
            tags: $content->getTags()->map(static fn (Tag $tag) => TagResponse::create($tag))->getValues(),
            createdAt: $content->getCreatedAt(),
            updatedAt: $content->getUpdatedAt(),
        );
    }
}
