<?php

declare(strict_types=1);

namespace UXF\Content\Http\Cms;

use UXF\Content\Entity\Author;
use UXF\Content\Entity\Category;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\ContentLite;
use UXF\Content\Entity\Seo;
use UXF\Content\Entity\Tag;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Core\Type\DateTime;
use UXF\Storage\Entity\Image;

final readonly class ContentCmsRequestBody
{
    /**
     * @param Tag[] $tags
     */
    public function __construct(
        public string $type,
        public string $name,
        public string $perex,
        public Seo $seo,
        public VisibilityLevel $visibilityLevel,
        public bool $hidden,
        public ?Image $image,
        public ?DateTime $publishedAt,
        public ?Author $author,
        public ContentLite $content,
        public array $tags,
        public ?Content $parent = null, // BC -> TODO set as required
        public ?Category $category = null,
    ) {
    }
}
