<?php

declare(strict_types=1);

namespace UXF\Content\Http;

enum ContentSort: string
{
    case TYPE = 'type';
    case NAME = 'name';
    case PUBLISHED_AT = 'publishedAt';
}
