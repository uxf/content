<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Author;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\Tag;

final readonly class ContentRequestQuery
{
    /**
     * @param string[] $types
     * @param Author[] $authors
     * @param Tag[] $tags
     */
    public function __construct(
        public array $types = [],
        public array $authors = [],
        public array $tags = [],
        public string $term = '',
        public ?Content $parent = null,
        public ContentSort $sort = ContentSort::PUBLISHED_AT,
        public ContentSortDir $dir = ContentSortDir::DESC,
        public int $limit = 20,
        public int $offset = 0,
    ) {
    }
}
