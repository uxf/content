<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Category;

// TODO Bee -> add more props - konzultace s FE
final readonly class CategoryResponse
{
    public function __construct(
        public int $id,
        public string $name,
    ) {
    }

    public static function create(Category $category): self
    {
        return new self(
            id: $category->getId(),
            name: $category->getName(),
        );
    }

    public static function createNullable(?Category $category): ?self
    {
        return $category !== null ? self::create($category) : null;
    }
}
