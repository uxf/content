<?php

declare(strict_types=1);

namespace UXF\Content\Http;

use UXF\Content\Entity\Author;
use UXF\Storage\Http\Response\ImageResponse;

final readonly class AuthorResponse
{
    public function __construct(
        public int $id,
        public string $firstName,
        public string $surname,
        public ?ImageResponse $avatar,
    ) {
    }

    public static function create(Author $author): self
    {
        return new self(
            id: $author->getId(),
            firstName: $author->getFirstName(),
            surname: $author->getSurname(),
            avatar: ImageResponse::createNullable($author->getAvatar()),
        );
    }

    public static function createNullable(?Author $author): ?self
    {
        return $author !== null ? self::create($author) : null;
    }
}
