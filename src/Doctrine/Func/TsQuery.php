<?php

declare(strict_types=1);

namespace UXF\Content\Doctrine\Func;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;

final class TsQuery extends FunctionNode
{
    private Node $column; // @phpstan-ignore-line
    private Node $string; // @phpstan-ignore-line

    public function getSql(SqlWalker $sqlWalker): string
    {
        $column = $this->column->dispatch($sqlWalker);
        $value = $this->string->dispatch($sqlWalker);

        return $sqlWalker->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform
            ? "{$column} @@ plainto_tsquery('unaccented_cs', {$value})"
            : "{$column} LIKE '%' || {$value} || '%'";
    }

    public function parse(Parser $parser): void
    {
        $parser->match(TokenType::T_IDENTIFIER);
        $parser->match(TokenType::T_OPEN_PARENTHESIS);
        $this->column = $parser->StringPrimary();
        $parser->match(TokenType::T_COMMA);
        $this->string = $parser->StringPrimary();
        $parser->match(TokenType::T_CLOSE_PARENTHESIS);
    }
}
