<?php

declare(strict_types=1);

namespace UXF\Content\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Tools\Event\GenerateSchemaTableEventArgs;
use Doctrine\ORM\Tools\ToolEvents;
use UXF\Content\Doctrine\Type\TsVector;

#[AsDoctrineListener(ToolEvents::postGenerateSchemaTable)]
class SearchColumnEventSubscriber
{
    public function __invoke(GenerateSchemaTableEventArgs $args): void
    {
        $metadata = $args->getClassMetadata();
        foreach ($metadata->fieldMappings as $fieldMapping) {
            if ($fieldMapping['type'] === TsVector::NAME) {
                $args->getClassTable()->addIndex(columnNames: [$fieldMapping['columnName']]);
            }
        }
    }
}
