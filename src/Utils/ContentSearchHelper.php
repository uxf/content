<?php

declare(strict_types=1);

namespace UXF\Content\Utils;

use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\Strings;

/**
 * @experimental
 */
final readonly class ContentSearchHelper
{
    public const string SIMPLE = 'simple';
    public const string WORDS = 'words';
    public const string WORDS_AUTOCOMPLETE = 'autocomplete';

    /**
     * @param string|string[] $paths
     * @param self::* $mode
     */
    public static function apply(QueryBuilder $qb, string|array $paths, string $term, string $mode = self::SIMPLE): QueryBuilder
    {
        $condition = self::prepare($qb, $paths, $term, $mode);
        if ($condition === null) {
            return $qb;
        }

        return $qb->andWhere($condition);
    }

    /**
     * @param string|string[] $paths
     * @param self::* $mode
     */
    public static function prepare(QueryBuilder $qb, string|array $paths, string $term, string $mode = self::SIMPLE): ?Orx
    {
        $term = trim($term);
        if ($term === '') {
            return null;
        }

        $parts = Strings::split($term, '/\s+/');
        $lastIndex = count($parts) - 1;

        $conditions = [];
        $paths = is_array($paths) ? $paths : [$paths];

        foreach ($parts as $index => $s) {
            $key = "search_$index";
            foreach ($paths as $path) {
                $conditions[$path][] = "$path LIKE UNACCENT(:$key)";
            }
            $qb->setParameter($key, self::resolveValue($s, $mode, $index === $lastIndex));
        }

        return new Orx(array_map(static fn (array $conds) => new Andx($conds), $conditions));
    }

    /**
     * @param self::* $mode
     */
    private static function resolveValue(string $value, string $mode, bool $lastWord): string
    {
        if ($mode === self::WORDS || ($mode === self::WORDS_AUTOCOMPLETE && !$lastWord)) {
            return "% $value %";
        }

        if ($mode === self::WORDS_AUTOCOMPLETE) {
            return "% $value%";
        }

        return "%$value%";
    }
}
