<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\Core\Test\Client;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();
    $parameters = $containerConfigurator->parameters();

    $services->defaults()
        ->autowire()
        ->autoconfigure()
        ->public();

    $services->set(Client::class);
    $services->alias('test.client', Client::class);

    $containerConfigurator->extension('framework', [
        'test' => true,
        'validation' => [
            'email_validation_mode' => 'html5',
        ],
        'http_method_override' => false,
    ]);

    $containerConfigurator->extension('security', [
        'firewalls' => [
            'secured' => [],
        ],
    ]);

    $parameters->set('env(TEST_DATABASE_URL)', __DIR__ . '/../../var/db.sqlite');

    $containerConfigurator->extension('doctrine', [
        'dbal' => [
            'url' => null,
            'driver' => 'pdo_sqlite',
            'path' => '%env(TEST_DATABASE_URL)%',
        ],
        'orm' => [
            'naming_strategy' => 'doctrine.orm.naming_strategy.underscore_number_aware',
            'auto_mapping' => true,
            'report_fields_where_declared' => true,
            'enable_lazy_ghost_objects' => true,
        ],
    ]);
};
