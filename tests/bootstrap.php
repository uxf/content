<?php

declare(strict_types=1);

use Symfony\Component\ErrorHandler\ErrorHandler;
use UXF\Core\SystemProvider\Calendar;
use UXF\Core\SystemProvider\Clock;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;

require __DIR__ . '/../../../vendor/autoload.php';

// https://github.com/symfony/symfony/issues/53812#issuecomment-1962311843
ErrorHandler::register(null, false);

Clock::$frozenTime = new DateTime('2022-01-01T10:00:00+01:00');
Calendar::$frozenDate = new Date('2022-01-01');
