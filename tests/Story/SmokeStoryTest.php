<?php

declare(strict_types=1);

namespace UXF\ContentTests\Story;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Content\Entity\Author;
use UXF\Content\Entity\Category;
use UXF\Content\Entity\ContentLite;
use UXF\Content\Entity\Seo;
use UXF\Content\Entity\Tag;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Content\Http\Cms\ContentCmsRequestBody;
use UXF\Core\Type\DateTime;
use function Safe\json_decode;
use function Safe\json_encode;

class SmokeStoryTest extends StoryTestCase
{
    public function test(): void
    {
        $client = self::createClient();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $entityManager->persist(new Tag('XXX', 'title1'));
        $entityManager->persist(new Tag('YYY', 'title2'));
        $entityManager->persist(new Category('Category 1'));
        $entityManager->persist($author = new Author('Joe', 'Doe'));
        $entityManager->persist($author2 = new Author('Joe', 'Smith'));
        $entityManager->flush();

        // create
        $body = new ContentCmsRequestBody(
            type: 'type',
            name: 'name',
            perex: "perex clanku",
            seo: new Seo(
                name: 'seoName',
                title: 'seoTitle',
                description: 'seoDescription',
                ogTitle: 'seoOgTitle',
                ogDescription: 'seoOgDescription',
                ogImage: null,
            ),
            visibilityLevel: VisibilityLevel::PUBLIC,
            hidden: true,
            image: null,
            publishedAt: new DateTime('2020-01-01'),
            author: $author,
            content: new ContentLite([], ''),
            tags: [],
            parent: null,
            category: null,
        );

        $dataCreate = json_decode(json_encode($body), true);
        $dataCreate['tags'] = [1];
        $dataCreate['author'] = 1;
        $dataCreate['category'] = 1;
        $dataCreate['content'] = [
            'data' => [[
                'type' => 'wysiwyg',
                'content' => [
                    'property' => 'value',
                ],
            ]],
            'search' => 'Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu.',
        ];

        $client->post('/api/cms/content', $dataCreate);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // duplicity seoName - error
        $client->post('/api/cms/content', $dataCreate);
        self::assertResponseStatusCodeSame(400);
        self::assertSame('seo.name', $client->getResponseData()['validationErrors'][0]['field']);

        // update
        $body = new ContentCmsRequestBody(
            type: 'XXXXX',
            name: 'name2',
            perex: "perex clanku 2",
            seo: new Seo(
                name: 'seoName2',
                title: 'seoTitle2',
                description: 'seoDescription2',
                ogTitle: 'seoOgTitle2',
                ogDescription: 'seoOgDescription2',
                ogImage: null,
            ),
            visibilityLevel: VisibilityLevel::PUBLIC_WITHOUT_SITEMAP,
            hidden: false,
            image: null,
            publishedAt: new DateTime('2020-01-02'),
            author: $author2,
            content: new ContentLite([], ''),
            tags: [],
            parent: null,
            category: null,
        );

        $dataUpdate = json_decode(json_encode($body), true);
        $dataUpdate['tags'] = [2];
        $dataUpdate['author'] = 2;
        $dataUpdate['category'] = 1;
        $dataUpdate['content'] = [
            'data' => [[
                'type' => 'wysiwyg',
                'content' => [
                    'property' => 'value2',
                ],
            ]],
            'search' => 'Příliš žluťoučký kůň úpěl ďábelské ódy.',
        ];

        $client->put('/api/cms/content/1', $dataUpdate);
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // duplicity seoName - error
        // create content id 2 with seo name wow
        $dataCreate['seo']['name'] = 'wow';
        $dataCreate['hidden'] = false;
        $dataCreate['parent'] = 1;
        $client->post('/api/cms/content', $dataCreate);
        self::assertResponseIsSuccessful();
        self::assertSame(1, $client->getResponseData()['parent']['id']);

        $dataUpdate['seo']['name'] = 'wow';
        $client->put('/api/cms/content/1', $dataUpdate);
        self::assertResponseStatusCodeSame(400);
        self::assertSame('seo.name', $client->getResponseData()['validationErrors'][0]['field']);

        // get public
        $client->get('/api/app/content/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        $client->get('/api/app/content?authors[]=1&tags[]=1&parent=1&sort=name&dir=asc');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // get all contents
        $client->get('/api/app/content');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // delete content
        $client->delete('/api/cms/content/1');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // get all contents
        $client->get('/api/app/content');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());

        // filter by term
        $client->get('/api/app/content?term=hrisne saxofony');
        self::assertResponseIsSuccessful();
        $this->assertSnapshot($client->getResponseData());
    }
}
