<?php

declare(strict_types=1);

namespace UXF\ContentTests\Story;

use Doctrine\ORM\EntityManagerInterface;
use UXF\Content\Entity\Content;
use UXF\Content\Entity\ContentLite;
use UXF\Content\Entity\Seo;
use UXF\Content\Entity\VisibilityLevel;
use UXF\Content\Utils\ContentSearchHelper;

class SearchStoryTest extends StoryTestCase
{
    public function test(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        $content = new Content(
            type: '',
            name: '',
            perex: '',
            seo: new Seo(
                name: null,
                title: '',
                description: '',
                ogTitle: '',
                ogDescription: '',
                ogImage: null,
            ),
            visibilityLevel: VisibilityLevel::PUBLIC,
            hidden: false,
            image: null,
            publishedAt: null,
            author: null,
            content: new ContentLite([], 'Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu. \'"`@#$~^&*{} -+123456/7890  '),
            tags: [],
            parent: null,
            category: null,
        );
        $entityManager->persist($content);
        $entityManager->flush();

        $path = 'content.content.textSearch';
        $path2 = 'content.content.textSearch2';
        $qb = $entityManager->createQueryBuilder()
            ->select($path)
            ->from(Content::class, 'content');

        // prepare - test empty term
        self::assertNull(ContentSearchHelper::prepare(clone $qb, $path, ''));

        // prepare - test non empty term
        self::assertNotNull(ContentSearchHelper::prepare(clone $qb, $path, 'x'));

        // test empty term
        $dql = ContentSearchHelper::apply(clone $qb, $path, '')->getQuery()->getDQL();
        self::assertSame('SELECT content.content.textSearch FROM UXF\Content\Entity\Content content', $dql);

        // test non empty term
        $dql = ContentSearchHelper::apply(clone $qb, $path, 'x')->getQuery()->getDQL();
        self::assertSame('SELECT content.content.textSearch FROM UXF\Content\Entity\Content content WHERE content.content.textSearch LIKE UNACCENT(:search_0)', $dql);

        // test non empty term
        $dql = ContentSearchHelper::apply(clone $qb, [$path, $path2], 'x y')->getQuery()->getDQL();
        self::assertSame('SELECT content.content.textSearch FROM UXF\Content\Entity\Content content WHERE (content.content.textSearch LIKE UNACCENT(:search_0) AND content.content.textSearch LIKE UNACCENT(:search_1)) OR (content.content.textSearch2 LIKE UNACCENT(:search_0) AND content.content.textSearch2 LIKE UNACCENT(:search_1))', $dql);

        // test simple
        $result = ContentSearchHelper::apply(clone $qb, $path, ' axofon Tang  jiz')->getQuery()->getSingleScalarResult();
        self::assertSame(' necht jiz hrisne saxofony dablu rozezvuci sin udesnymi tony waltzu tanga a quickstepu. @ -+123456/7890 ', $result);

        // test words
        $result = ContentSearchHelper::apply(clone $qb, $path, 'SAXOFONY SIN', ContentSearchHelper::WORDS)->getQuery()->getResult();
        self::assertNotEmpty($result);

        // test words miss
        $result = ContentSearchHelper::apply(clone $qb, $path, 'saxofon', ContentSearchHelper::WORDS)->getQuery()->getResult();
        self::assertEmpty($result);

        // test autocomplete
        $result = ContentSearchHelper::apply(clone $qb, $path, 'tony rozez ', ContentSearchHelper::WORDS_AUTOCOMPLETE)->getQuery()->getResult();
        self::assertNotEmpty($result);
    }
}
